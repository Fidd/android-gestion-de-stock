<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'prefix' => 'auth'
], function ($router) {
Route::post("login", "AuthController@userLogin");
    Route::post("register", "AuthController@register");
    Route::post('logout', 'AuthController@logout');
    Route::get('me', 'AuthController@me');
});

Route::group([
    'namespace' => 'Utils'
], function ($router) {
    Route::post('store-picture', 'ImageController@store');
});

Route::group([
    'middleware' => ['sanctum.verify']
], function ($router){    
    Route::get('products', 'AdminController@getAllProducts');
    Route::get('products/{id}', 'AdminController@getProduct');
    Route::post('products/{id}', 'AdminController@updateProduct');
    Route::delete('products/{id}', 'AdminController@deleteProduct');
    Route::post('create-product', 'AdminController@store');
    Route::post('create-categorie','AdminController@storeCategories');
});
