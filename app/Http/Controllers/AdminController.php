<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Utils\JsonResponse;
use App\Product;
use App\Categorie;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    /**
    * Create a product
    * @bodyParam name string required  Product's name. Example:
    * @bodyParam description string required  The product's description. Example:Un simple produit d'utilisation
    * @bodyParam categorie string required  The categorie of product. Example:Electromenagers
    * @bodyParam quantity number required|min:5  Quantity if product. Example: 10
    */
    public function store(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'description' => 'required',
                'categorie' => 'required',
                'quantity' => 'required'
            ]
        );
        if($validator->fails()){
            $response = new JsonResponse();
            $response->fail($validator->errors());
            return response()->json($response);
        }
        $categorie=DB::table('categories')->where('name',$request->input('categorie'))->first();
        if(!$categorie){
            $response = new JsonResponse();
            $response->fail(['echec'=>'la categorie \'existe pas']);
            return response()->json($response);
        }

        $data=array_merge($request->only('name','description','quantity'),['categorie_id' => $categorie->id]);
        $products=Product::insert($data);

        if($products){
            $response = new JsonResponse();
            $response->success(["ok"=>"saving done"]);
            return response()->json($response);
        }else{
            $response = new JsonResponse();
            $response->fail(["echec"=>"echec d'enregistrement"]);
            return response()->json($response);
        }
    }

    /**
    * Create a Categorie
    * @bodyParam name string required  Name of the Categorie. Example:
    */
    public function storeCategories(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required'
            ]
        );
        if($validator->fails()){
            $response = new JsonResponse();
            $response->fail($validator->errors());
            return response()->json($response);
        }
        $categorie = Categorie::insert($request->all());

        if($categorie){
            $response = new JsonResponse();
            $response->success(["success"=>"Catégorie enregistrée"]);
            return response()->json($response);
        }
    }

    public function getAllProducts(){
        $products=Product::get();
        $response = new JsonResponse();
        $response->success($products);
        return response()->json($response);
    }
    /**
    * @authenticated
    * Get all product of the Admin
    * @response {
    *    "success": true,
    *    "data":[] {
    *       "id": 1,
    *        "name": "Firstly",
    *       "description": "Un petit couteau",
    *       "image_path": null,
    *      "quantity": 6,
    *        "categorie_id": 2,
    *       "created_at": null,
    *       "updated_at": "2021-02-18T16:08:10.000000Z"
    *       },
    *       {
    *       "id": 2,
    *        "name": "Firstly",
    *       "description": "Un autre petit couteau",
    *       "image_path": null,
    *      "quantity": 6,
    *        "categorie_id": 2,
    *       "created_at": null,
    *       "updated_at": "2021-02-18T16:08:10.000000Z"
    *    },
    *     ],
    *    "error": "",
    *    "code": 200
    *}
    */

    public function getProduct($id){
        if(Product::where('id',$id)->exists()){
            $product=Product::where('id',$id)->first();

            // C'est pas obligatoire mais je l'ai fait
            $cat=Categorie::where('id',$product->categorie_id)->first();
            $data=[
                'id'=>$product->id,
                'name'=>$product->name,
                'description'=>$product->description,
                'image_path'=>$product->image_path,
                'quantity'=>$product->quantity,
                'categorie'=>$cat->name
            ];
            $response = new JsonResponse();
            //we can change $data by $product
            $response->success($data);
            return response()->json($response);
        }else{
            $response = new JsonResponse();
            $response->fail(["echec"=>"product not exist"]);
            return response()->json($response);
        }
    }
    /**
    * @authenticated
    * Get a specific product the Admin
    * @response {
    *    "success": true,
    *    "data":[] {
    *       "id": 1,
    *        "name": "Firstly",
    *       "description": "Un petit couteau",
    *       "image_path": null,
    *      "quantity": 6,
    *        "categorie_id": 2,
    *       "created_at": null,
    *       "updated_at": "2021-02-18T16:08:10.000000Z"
    *       }
    *    "error": "",
    *    "code": 200
    *}
    */

    /**
    * delete a product
    * @bodyParam id number id of product to delete. Example: 10
    */
    public function deleteProduct($id){
        if(Product::where('id',$id)->exists()){
            $product=Product::find($id);
            $product->delete();
            $response = new JsonResponse();
            $response->success(['ok'=>"deleted"]);
            return response()->json($response);
        }else{
            $response = new JsonResponse();
            $response->fail(['sorry'=>"not exist"]);
            return response()->json($response);
        }
    }

    /**
    * update a product
    * @bodyParam name string  Product's name. Example:Veentillo
    * @bodyParam description string The New product's description. Example:Un simple produit d'utilisation
    * @bodyParam image_path string The New categorie of product. Example:Electromenagers
    * @bodyParam quantity number New Quantity if product. Example: 10
    * @bodyParam id number id of product to update. Example: 10
    */
    public function updateProduct(Request $request , $id){
        if(Product::where('id',$id)->exists()){
            $product=Product::find($id);
            $product->name=is_null($request->input('name')) ? $product->name : $request->input('name');
            $product->description=is_null($request->input('description')) ? $product->description : $request->input('description');
            $product->quantity=is_null($request->input('quantity')) ? $product->quantity : $request->input('quantity');
            $product->image_path=is_null($request->image_path) ? $product->image_path : $request->input('image_path');
            $product->save();
            $response = new JsonResponse();
            $response->success(['ok'=>'updating done well']);
            return response()->json($response);
        }else{
            $response = new JsonResponse();
            $response->fail(['sorry'=>"not exist"]);
            return response()->json($response);
        }
    }
}
